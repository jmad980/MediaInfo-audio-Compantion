#!/bin/bash
set -eo pipefail
IFS=$'\n\t'
#        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

function commandDispatch () {
    local command=$(tr '[:upper:]' '[:lower:]' <<< "$1")
    case "$command" in
    "tracklist")
    makeTracklist
    ;;
    "artists")
    showArtistList
    ;;
    "summary")
    showSummary
    ;;
    "all")
    runAllCommands
    ;;
    *)
    printf "Command Not Found.\n"
    exit
    ;;
    esac
}

function sillyHeader () {
    printf "%0.s-" $(seq 1 ${#1})
    printf "\n%s\n" "$1"
    printf "%0.s-" $(seq 1 ${#1})
    printf "\n"
}
function runAllCommands () {
    sillyHeader "-Tracklist-"
    makeTracklist

    sillyHeader "-Artists-"
    showArtistList

    sillyHeader "-Summary-"
    showSummary
}

function convertSecToHuman () {
    ((h=${1}/3600))
    ((m=(${1}%3600)/60))
    ((s=${1}%60))
    if (( "$h" != 0 )); then
        printf "%02d:%02d:%02d\n" $h $m $s
    else
        printf "%02d:%02d\n" $m $s
    fi
}

function genArtistsArray () {
    # Don't regen array if not empty
    if (( ${#artists[@]} == 0 )); then
        artist_list=$(mediainfo --Inform="General;%Artist%\n" *.$file_format | sort -u | uniq)

        while read a;
        do
            if [[ ! -z "$a" ]]; then
                artists+=("$a")
            fi
        done <<< "$artist_list"
    fi
}


function showArtistList () {
    genArtistsArray
    for a in "${artists[@]}"; do
        printf "%s\n" "${a}"
    done
}


function checkPathLengths () {
    PATH_TOO_LONG_FILES=()
    declare -i MAX_PATH_LENGTH
    MAX_PATH_LENGTH=180

    for a in *; do
        local path_name="${PWD##*/}/${a}"
        if (( "${#path_name}"  > $MAX_PATH_LENGTH )); then
            PATH_TOO_LONG=true
            How_many_extra=$((${#path_name} - $MAX_PATH_LENGTH))
            main_dir="${PWD##*/}"
            if (( "${#main_dir}" >= $MAX_PATH_LENGTH )); then
                PATH_TOO_LONG_FILES+=("Folder Name > $MAX_PATH_LENGTH characters, consider renaming folder")
                break
            fi

            extension="${a##*.}"
            extoffset=$(("${#extension}" + 1))
            if [[ "$extension" == "$a" ]]; then
                good_part="${a:0: -$How_many_extra}"
                bad_part="${a:${#a} - $How_many_extra}"
                colorit="${good_part}\033[0;31m${bad_part}\033[0m"
            elif (( $(("${How_many_extra}" + "${extoffset}")) < "${#a}" )); then
                good_part="${a:0: -(($How_many_extra + $extoffset))}"
                bad_part="${a:((${#a} - $How_many_extra - $extoffset))}"
                bad_part_noext="${bad_part:0:((${#bad_part} - $extoffset))}"
                colorit="${good_part}\033[0;31m${bad_part_noext}\033[0m.${extension}"
            else
                PATH_TOO_LONG_FILES+=("(Path Length Too Long, consider renaming folder): ${a}")
                continue
            fi
            PATH_TOO_LONG_FILES+=("($(($How_many_extra + $MAX_PATH_LENGTH)) > $MAX_PATH_LENGTH): $colorit")
        fi
    done
}

function showHelp () {
    printf "mic.sh ${VERSION}\n"
    printf 'Usage:\tmic.sh [options] command\n'
    printf 'Commands:\n'
    printf "\t%s\n" 'summary | Show Artists amount, Embeded Art Status/Size, Check Path Lengths'
    printf "\t%s\n" 'artists | List artists based on tags'
    printf "\t%s\n" 'tracklist | Generate BBcode formatted tracklist'
    printf 'options:\n'
    printf "\t%s\n" '--compilation | Add Artists to Tracklist'
    printf "\t%s\n" '--format=* | (mp3 or flac) Overide detected format.'
    printf "\t%s\n" '--help|-h | Show this help'
}

function showSummary () {
    file_list=(*.${file_format})
    genArtistsArray
    checkPathLengths
    has_cover=$(mediainfo --inform="General;%Cover%" "${file_list[0]}")
    image_data=$(mediainfo --Cover_Data=base64 --inform="General;%Cover_Data%" "${file_list[0]}" | base64 -d |  wc -c)
    Artsize=$(( "$image_data" / 1000 ))

    if [[ "$PATH_TOO_LONG" == true ]]; then
        printf "\033[0;31mFile Paths Too Long!!! (>180 chars):\033[0m\n"
        for i in "${PATH_TOO_LONG_FILES[@]}"
        do
           printf "\t* ${i}\n"
        done
    fi

    if (( $Artsize > 500 )); then
        art_status_color="\033[0;31m"
    else
        art_status_color="\033[0;32m"
    fi

    if [[ $PATH_TOO_LONG == true ]]; then
        path_status_color="\033[0;31m"
    else
        path_status_color="\033[0;32m"
    fi

    printf "Artist Count: ${#artists[@]} \nEmbeded Art: ${has_cover} (${art_status_color}${Artsize}\033[0mKB) \nPaths too Long?: ${path_status_color}${PATH_TOO_LONG}\033[0m\n"
}

function makeTracklist () {
    file_list=(*.${file_format})
    
    album_info=$(mediainfo --Inform="General;%Album%\n%Recorded_Date%" "${file_list[0]}")
    album_name=$(head -n1 <<< "$album_info")
    album_year=$(tail -n1 <<< "$album_info")

    genArtistsArray
    if (( ${#artists[@]} == 1 )); then
        printf "%s\n\n" "[size=5][b][artist]${artists[0]}[/artist] - ${album_name}[/b][/size]"
    elif (( ${#artists[@]} == 2 )); then
        printf "%s\n\n" "[size=5][b][artist]${artists[0]}[/artist] & [artist]${artists[1]}[/artist] - ${album_name}[/b][/size]"
    else
        printf "%s\n\n" "[size=5][b]Various Artists - ${album_name}[/b][/size]"
    fi

    printf "%s\n\n" "[b]Year:[/b] ${album_year}"

    printf "[size=4][b]Tracklist[/b][/size]\n"
    declare -i total_ms=0

    for a in *.${file_format}; do
        if [[ "$isCompilation" == true ]]; then
            mediainfo_format=$(mediainfo --Inform="General;\[b]%Track/Position%.\[/b] \[artist]%Artist%\[/artist] - %Track%\n%Duration%" "$a")
        else
            mediainfo_format=$(mediainfo --Inform="General;\[b]%Track/Position%.\[/b] %Track%\n%Duration%" "$a")
        fi

        track_ms=$(tail -n1 <<< "$mediainfo_format")
        total_ms=$(( $total_ms + $track_ms ))
        track_seconds=$(awk '{printf "%.0f", $1 / 1000}' <<< " $track_ms")

        printf "%s\n" "$(head -n1 <<< "${mediainfo_format}") [i]($(convertSecToHuman $track_seconds))[/i]"
    done
    printf "\n[b]Total length[/b]: $(convertSecToHuman $(awk '{printf "%.0f", ($1 / 1000)}' <<< " $total_ms"))\n"
}

# Declare outside of function to make global
declare -a artists
isCompilation=false
PATH_TOO_LONG=false

file_format=""

mp3_files=(`find ./ -maxdepth 1 -name "*.mp3"`)
if [ ${#mp3_files[@]} -gt 0 ]; then
    file_format="mp3"
else
    file_format="flac"
fi

for i in "$@"; do
case $i in
    -h|--help)
    showHelp
    exit
    ;;
    --format=*)
    file_format="${i#*=}"
    shift
    ;;
    --compilation)
    isCompilation=true
    shift
    ;;
    *)
    commandDispatch "${i}"
    ;;
esac
done
